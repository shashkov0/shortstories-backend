import * as user from './user'
import * as story from './story'
import * as genre from './genre'
import * as storyStats from './story-stats'
import * as like from './like'
import * as dislike from './dislike'
import * as view from './view'
import * as comment from './comment'

export default { user, story, genre, storyStats, like, dislike, view, comment }
