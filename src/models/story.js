import VK from 'vk-io'
import slugify from '@sindresorhus/slugify'
import models from '.'

const story = (sequelize, DataTypes) => {
  const Story = sequelize.define('story', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [1800, 40000],
      },
    },
    length: {
      type: DataTypes.INTEGER,
    },
  })

  Story.associate = models => {
    Story.belongsTo(models.User)
    Story.belongsTo(models.Genre)
    Story.hasMany(models.Comment, { onDelete: 'cascade', hooks: true })
    Story.hasMany(models.Reaction, { onDelete: 'cascade', hooks: true })
    Story.hasMany(models.View, { onDelete: 'cascade', hooks: true })
  }

  Story.beforeCreate(async story => {
    story.length = story.body.length
  })

  if (process.env.NODE_ENV === 'production') {
    Story.afterCreate(async story => {
      const genre = await models.Genre.findByPk(story.genreId)
      const vk = new VK({
        language: 'ru',
      })
      vk.token = process.env.VK_TOKEN
      await vk.api.wall.post({
        owner_id: -parseInt(process.env.VK_APP_ID, 10),
        message: `
        ${story.title}
  
        ${story.body.slice(0, 1000)}...
  
        Читать полностью: https://shortstories.io/story/${story.id}-${slugify(
          story.title
        )}
  
        #${genre.name.replace(' ', '_').replace('+', 'плюс')}@shortstories_io
        `,
      })
    })
  }

  return Story
}

export default story
